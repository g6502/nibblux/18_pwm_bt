library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity PWM_BT is
	port(	clk	: in	std_logic;
			rx		: in	std_logic;
			busy	: out	std_logic;
			pwm_o	: out	std_logic
	);
end PWM_BT;

architecture behave of PWM_BT is

	signal byte	: std_logic_vector(7 downto 0);
--	signal pwm_2	: std_logic;

begin
	
	u0	: entity work.UART_RX
		port map(	
						clk	=> clk,
						rx		=> rx,
						busy	=> busy,
						byte	=> byte);

	u1	: entity work.PWM_nbits
		generic map(
						division	=> 100,
						n_bits	=> 8)
		port map(
						clk	=> clk,
--						ciclo_util	=> byte - 40,
						ciclo_util	=> byte - 64,
						pwm_o	=>	pwm_o);
end behave;
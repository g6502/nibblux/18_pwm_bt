library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;


entity PWM_nbits is
	generic(
		frec_clk	: integer := 50_000_000;
		periodo_pwm	: integer := 1000;
		division	: integer := 10000;
		n_bits	: integer := 16
	);
	port(
		clk	: in	std_logic;
		ciclo_util	: in std_logic_vector(n_bits-1 downto 0);
		pwm_o	: out	std_logic
	);

end PWM_nbits;

architecture behavioral of PWM_nbits is
	signal frec	: std_logic;
	signal cont	: std_logic_vector(n_bits-1 downto 0) := (others => '0');
	--signal cont	: std_logic_vector(ciclo_util'length) := (others => '0');
begin
	u0	: entity work.div_freq
		generic map(fin_cont => frec_clk/periodo_pwm/2/division)
		port map(clk => clk,
					dclk => frec);
	process(frec)
	begin
		if rising_edge(frec) then
			if cont = std_logic_vector(to_unsigned(division, cont'length)) then
				cont <= (others => '0');
			else
				cont <= cont + 1;
			end if;
		end if;
	end process;
	pwm_o <= '1' when cont < ciclo_util else '0';
end behavioral;